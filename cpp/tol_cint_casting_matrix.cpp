/* -*- mode: C++ -*- */
//////////////////////////////////////////////////////////////////////////////
// FILE   : tol_cint_casting_matrix.cpp
// PURPOSE: Defines C/C++ casting for Matrix TOL type
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
class Matrix
//////////////////////////////////////////////////////////////////////////////
{
public:
  int rows_;
  int columns_;
  double* buffer_;
  int deleteBuffer_;

  Matrix(int rows, int columns, double* buffer, int deleteBuffer=0)
  : rows_(rows),
    columns_(columns),
    buffer_(buffer),
    deleteBuffer_(deleteBuffer)
  {}

  Matrix(int rows, int columns)
  : rows_(0),
    columns_(0),
    buffer_(NULL),
    deleteBuffer_(false)
  {
    Alloc(rows,columns);
  }

  Matrix(const Matrix& mat)
  : rows_(0),
    columns_(0),
    buffer_(NULL),
    deleteBuffer_(false)
  {
    Copy(mat);
  }

 ~Matrix()
  {
    Free();
  }

  void Free()
  {
    if(deleteBuffer_ && buffer_) delete  [] buffer_;
    buffer_ = NULL;
    rows_ = 0;
    columns_ = 0;
    deleteBuffer_ = false;
  }

  void Alloc(int rows, int columns)
  {
    Free();
    rows_ = rows;
    columns_ = columns;
    buffer_ = new double[rows*columns];
    deleteBuffer_ = true;
  }

  void Copy(const Matrix& mat)
  {
    Alloc(mat.rows_, mat.columns_);
    memcpy(buffer_,mat.buffer_,rows_*columns_*sizeof(double));
  }

  const Matrix& operator = (const Matrix& mat)
  {
    Copy(mat);
    return *this;
  }

//Zero based vector access
  double& operator[] (int k) const
  {
    return(buffer_[k]);
  }
/*
//Zero based matrix access
  double& operator[] (int i, int j) const
  {
    return(buffer_[i*columns_+j]);
  }
*/
//One based vector access
  double& operator() (int k) const
  {
    return(buffer_[k-1]);
  }
//One based matrix access
  const double& operator() (int i, int j) const
  {
    int k = (i-1)*columns_+j-1;
  //printf("TRACE [Matrix::operator() (int i=%ld, int j=%ld) const k=%ld\n",i,j,k);
    return(buffer_[k]);
  }
//One based matrix access
  double& operator() (int i, int j)
  {
    int k = (i-1)*columns_+j-1;
  //printf("TRACE [Matrix::operator() (int i=%ld, int j=%ld) k=%ld\n",i,j,k);
    return(buffer_[k]);
  }
};
