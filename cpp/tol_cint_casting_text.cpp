/* -*- mode: C++ -*- */
//////////////////////////////////////////////////////////////////////////////
// FILE   : tol_cint_castingtext.cpp
// PURPOSE: Defines C/C++ casting for TOL Text type 
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
class Text
//////////////////////////////////////////////////////////////////////////////
{
public:
  int length_;
  char* buffer_;
  int deleteBuffer_;

  Text(int length, char* buffer, int deleteBuffer=0)
  : length_(length),
    buffer_(buffer),
    deleteBuffer_(deleteBuffer)
  {}

  Text(const char* buffer)
  : length_(0),
    buffer_(NULL),
    deleteBuffer_(0)
  {
    Copy(buffer);
  }

  Text(const Text& txt)
  : length_(0),
    buffer_(NULL),
    deleteBuffer_(0)
  {
    Copy(txt);
  }

 ~Text() 
  {
    Free();
  }

  void Free()
  {
    if(deleteBuffer_ && buffer_) delete  [] buffer_;
    length_ = 0;
    buffer_ = NULL;
    deleteBuffer_ = false;
  }

  void Alloc(int length)
  {
    Free();
    length_ = length;
    buffer_ = new char[length_+1];
    deleteBuffer_ = true;
  }

  void Copy(const char* buffer)
  {
    Alloc(strlen(buffer));
    strncpy(buffer_,buffer,length_+1);
  }

  void Copy(const Text& txt)
  {
    Alloc(txt.length_);
    strncpy(buffer_,txt.buffer_,length_+1);
  }

  const Text& operator = (const char* buffer)
  {
    Copy(buffer);
    return *this;
  }
  const Text& operator = (const Text& txt)
  {
    Copy(txt);
    return *this;
  }

//Zero based vector access
  char& operator[] (int i) const
  {
    return(buffer_[i]);
  }
//One based vector access
  char& operator() (int i) const
  {
    return(buffer_[i-1]);
  }
//concat operations
  Text operator + (const Text& right)
  {
    int len = length_ + right.length_;
    char* buf = new char[len+1];
    sprintf(buf,"%s%s",buffer_,right.buffer_);
    Text txt(len, buf, 1);
    return(txt);
  }
  Text operator + (const char* right)
  {
    int len = length_ + strlen(right);
    char* buf = new char[len+1];
    sprintf(buf,"%s%s",buffer_,right);
    Text txt(len, buf, 1);
    return(txt);
  }
  Text operator + (int num)
  {
    int maxlen = length_ + 64;
    char* buf = new char[maxlen+1];
    sprintf(buf,"%s%ld",buffer_,num);
    Text txt(strlen(buf), buf, 1);
    return(txt);
  }
  Text operator + (double num)
  {
    int maxlen = length_ + 64;
    char* buf = new char[maxlen+1];
    sprintf(buf,"%s%.16lg",buffer_,num);
    Text txt(strlen(buf), buf, 1);
    return(txt);
  }
};


