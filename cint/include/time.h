#ifndef G__TIME_H
#define G__TIME_H
typedef unsigned long clock_t;
typedef struct time_t {
  long l,u;
  time_t(long i=0){l=i;u=0;}
  void operator=(long i){l=i;u=0;}
} time_t;
#pragma link off class time_t;
#pragma link off typedef time_t;
#ifndef G__STDSTRUCT
#pragma setstdstruct
#endif
#define 	CLK_TCK (1000)
#endif
